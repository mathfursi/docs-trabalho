# Gerar um módulo
Vá até a pasta Hybris/hybris/bin/platform, abra o terminal e rode o seguinte comando:
```sh
$ ant modulegen
```
Digite develop ou apenas aperte ENTER para selecionar o valor padrão:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/selecione_opcao_ant_modulegen_1.png?raw=true)

Digite accelerator ou apenas aperte ENTER para selecionar o valor padrão:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_2.png?raw=true)

Digite o nome desejado para o seu módulo ou apenas aperte ENTER para selecionar o valor padrão de training:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_3.png?raw=true)

Digite o nome do pacote base para extensões ou apenas aperte ENTER para selecionar o valor padrão (recomendado deixar o valor padrão):
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_4.png?raw=true)

Caso tenha ocorrido tudo certo, a seguinte mensagem deve aparecer:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_5.png?raw=true)

# Inicializar o Hybris
Após ter criado o pacote para edição, deve-se inicialiar o projeto.
Para inicializá-lo use o seguinte comando:
```sh
$ ant initialize
```

# Compilar o Hybris
Checa a estrutura da pasta e builda caso não exista build.
Vá até a pasta Hybris/hybris/bin/platform, abra o terminal e rode o seguinte comando:
```sh
$ ant clean all
```

Caso dê tudo certo, deve aparecer a seguinte tela:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/finished_clean_all_1.png?raw=true)

# Inicializar o servidor
Após compilar o projeto, inicialize o servidor.
Para Linux, use o seguinte comando na pasta Hybris/hybris/bin/platform:
```sh
$ ./hybrisserver.sh
```
Para Windows, use o seguinte comando na pasta Hybris/hybris/bin/platform:
```sh
$ hybrisserver.bat
```