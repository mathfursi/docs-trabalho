# Criando um itemtype e inserindo no banco de dados
O código a seguir mostra como criar uma tabela. Para criar a tabela é necessário acessar o arquivo `seuprojeto-items.xml` que fica localizado em `C:/Hybris/hybris/bin/custom/seuprojeto/seuprojetocore`.

```html
  <itemtype code = "ItemTeste" autocreate = "true" generate = "true">
            <deployment table="IntegrationSystemCredentials" typecode="11000" />
            <attributes>
                <attribute qualifier = "customerID" type = "java.lang.String">
                    <modifiers read = "true" write = "true" search = "true" optional = "true"/>
                    <persistence type = "property"/>
                </attribute>
            </attributes>
        </itemtype>
```

* ##### **<itemtype>**
    * **code**: dado ao item, que será uma tabela no banco de dados.
    * **autocreate**: cria uma nova entrada no banco de dados.
    * **generate**: gera uma jalo class.
* ##### **<deployment>**
    * **table**: o nome da tabela onde as instâncias do item serão armazenadas.
    * **typecode**: número único usado pelo Hybris para referenciar o item.
* ##### **<attribute>**
    * **autocreate**: cria uma nova entrada no banco de dados.
    * **qualifier**: nome do atributo.
    * **type**: tipo de variável do atributo.
* ##### **<modifiers>**
    * **read**: permite ou não a leitura do atributo, gerando método getter ou não.
    * **write**: permite ou não a escrita do atributo, gerando método setter ou não.
    * **search**: pesquisável usando a FlexibleSearch.
    * **optional**: item não obrigatório.
 * ##### **<persistence>**
    * **type**: *property* ou *dynamic*. *Property* o valor será armazenado no banco e é chamado atributo persistente. *Dynamic* o valor não será armazenado no banco e é chamado de atributo dinâmico. 
    

# Importar projeto Hybris no IntelliJ

### 1. Selecione a opção "Import Project"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_1.png?raw=true)
### 2. Selecione a pasta onde o projeto está armazenado
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_2.png?raw=true)
### 3. Selecione o modelo "hybris"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_3.png?raw=true)
### 4. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_4.png?raw=true)
### 5. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_5.png?raw=true)
### 6. Marque as opções como está mostrado
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_6.png?raw=true)
### 7. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_7.png?raw=true)
### 8. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_8.png?raw=true)
### 9. Caso não haja SDK selecionado, cliquei no + e selecione a pasta onde o JDK está instalado
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_9.png?raw=true)

# Gerar um módulo
Vá até a pasta Hybris/hybris/bin/platform, abra o terminal e rode o seguinte comando:
```sh
ant modulegen
```
Digite develop ou apenas aperte ENTER para selecionar o valor padrão:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/selecione_opcao_ant_modulegen_1.png?raw=true)

Digite accelerator ou apenas aperte ENTER para selecionar o valor padrão:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_2.png?raw=true)

Digite o nome desejado para o seu módulo ou apenas aperte ENTER para selecionar o valor padrão de training:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_3.png?raw=true)

Digite o nome do pacote base para extensões ou apenas aperte ENTER para selecionar o valor padrão (recomendado deixar o valor padrão):
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_4.png?raw=true)

Caso tenha ocorrido tudo certo, a seguinte mensagem deve aparecer:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_5.png?raw=true)