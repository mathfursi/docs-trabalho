## Instalando e utilizando ambiente Hybris

Pré-requisitos:

* HYBRISCOMM[Versão].zip
* JRebel [Necessário ter uma licença para ativação]
* Apache Ant - [Baixar](https://ant.apache.org/bindownload.cgi)
* IDE [Intellij, Eclipse ou outras]
* Java JDK - [Baixar](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## Passo a passo:
1. Criar uma pasta com o nome que desejar [Recomendamos o nome da pasta como Hybris] no disco `C:`;
2. Extrair o arquivo HYBRISCOMM;
3. Abrir o **CMD** e ir até o caminho `C:\Hybris\hybris\bin\platform`;
4. Executar os seguintes comandos no terminal:
    . `ant modulegen` [Gera um módulo]
    . `ant initialize` [Inicializa o Hybris do zero]
    . `ant clean all` [Compila o projeto]
5. Execute  o arquivo `hybrisserver.bat` no mesmo diretório [ vai iniciar o servidor`localhost:9002`]


**Outros Comandos**:
`ant all` - Compila o projeto com base no build.xml que já existe
`ant build`- ???
`ant update` - atualizar o sistema com as informações adicionadas


#### Criando Tabelas 
O código a seguir mostra como criar uma tabela. Para criar a tabela é necessário acessar o arquivo `projeto-items.xml` que fica localizado em `HYBRIS/hybris/bin/custom/projeto/projetocore`.

```HTML
  <itemtype code = "CustomerTest"
                  autocreate = "true" generate = "true">
            <attributes>
                <attribute autocreate = "true" qualifier = "customerID" type = "java.lang.String">
                    <modifiers read = "true" write = "true" search = "true" optional = "true"/>
                    <persistence type = "property"/>
                </attribute>
            </attributes>
        </itemtype>
```

## Impex & Flexible Query
##### Impex
O impex é a menira que utilizamos para inserir, deletar ou alterar dados nas tabelas do projeto. Logo abaixo estão alguns exemplos de como utilizar cada recurso.
* INSERT -- Insere uma nova tupla na tabela, mas nesse caso uma `Exception` será lançada caso já exista uma tupla com os mesmos valores.

    ``` sql
    INSERT CustomerTest; CustomerID; 
    ;10;
    ```

* UPDATE -- Atualiza uma tupla já existente na tabela.
     ``` sql
    UPDATE CustomerTest; CustomerID; 
    ;10;
    ```
* INSERT_UPDATE -- Insere uma nova tupla na tabela caso o campo não exista ou atualiza a tupla já existente.
     ``` sql
    INSERT_UPDATE CustomerTest; CustomerID; 
    ;10;
    ```
##### Flexible Search
O Flexible Search é utilizado para retornar os valores inseridos nas tabelas, ou seja, ele é o `SELECT` do `SQL`.

Exemplo:
```sql
    SELECT * FROM {Customer};
```
**Observação:** O uso das *chaves* {} é necessário.