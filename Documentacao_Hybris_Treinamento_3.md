# Criar uma extensão
##### 1. Abra o terminal ou cmd e vá até a pasta `CAMINHO_PARA_O_HYBRIS/hybris/bin/platform`.
Digite o comando:
```sh
ant extgen
```
##### 2. Selecione o template ybackoffice e aperte "ENTER":
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_1.png?raw=true)
##### 3. Dê um nome para sua extensão e aperte "ENTER":
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_2.png?raw=true)
##### 4. Escolha o pacote para sua extensão e aperte "ENTER":
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_3.png?raw=true)
##### 5. Não registre como uma extensão SAAS:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_4.png?raw=true)
##### 6. Não crie um widget:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_5.png?raw=true)
##### 7. Não crie uma planilha simples de estilo:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_6.png?raw=true)

##### 8. Adicione a extensão no `localextensions.xml` em `CAMINHO_PARA_O_HYBRIS/hybris/config/localextensions.xml`:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_7.png?raw=true)

##### 9. Adicione a seguinte configuração no arquivo `CAMINHO_PARA_O_HYBRIS/hybris/bin/custom/extensao1/resources/NOME_DA_SUA_EXTENSAO-backoffice-config.xml`, substituindo os valores **"navigation-node id"**, **"type-node id"** e **"type-node code"** com os valores desejados:
```html
<context component="explorer-tree" merge-by="module">
		 <n:explorer-tree xmlns:n="http://www.hybris.com/cockpitng/config/explorertree">
			 <n:navigation-node id="ExtensaoTesteBackOffice">
			 <n:type-node id="Teste" code="Teste" />
			 </n:navigation-node>
		 </n:explorer-tree>
	</context>
	<context type="Teste" component="listview">
		 <list:list-view xmlns:list="http://www.hybris.com/cockpitng/component/listView">
			<list:column qualifier="code"/>
			<list:column qualifier="name"/>
			<list:column qualifier="duration"/>
			<list:column qualifier="amount"/>
		 	</list:list-view>
	</context>
```
Insira dentro da tag **<config>** como mostrado na imagem abaixo:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_8.png?raw=true)

##### 10. Execute os seguintes comandos na pasta `CAMINHO_PARA_O_HYBRIS/hybris/bin/platform`:
```sh
setantenv.bat
```
```sh
ant clean all
```
```sh
hybrisserver.bat
```
##### 11. Abra o https://localhost:9002 -> platform -> update (Selecione somente a extensão "extensao1" na lista) -> update
&nbsp;