# Criando um itemtype e inserindo no banco de dados
O código a seguir mostra como criar uma tabela. Para criar a tabela é necessário acessar o arquivo `seuprojeto-items.xml` que fica localizado em `C:/Hybris/hybris/bin/custom/seuprojeto/seuprojetocore`.

```html
  <itemtype code = "ItemTeste" autocreate = "true" generate = "true">
            <deployment table="IntegrationSystemCredentials" typecode="11000" />
            <attributes>
                <attribute qualifier = "customerID" type = "java.lang.String">
                    <modifiers read = "true" write = "true" search = "true" optional = "true"/>
                    <persistence type = "property"/>
                </attribute>
            </attributes>
        </itemtype>
```

* ##### **<itemtype>**
    * **code**: dado ao item, que será uma tabela no banco de dados.
    * **autocreate**: cria uma nova entrada no banco de dados.
    * **generate**: gera uma jalo class.
* ##### **<deployment>**
    * **table**: o nome da tabela onde as instâncias do item serão armazenadas.
    * **typecode**: número único usado pelo Hybris para referenciar o item.
* ##### **<attribute>**
    * **autocreate**: cria uma nova entrada no banco de dados.
    * **qualifier**: nome do atributo.
    * **type**: tipo de variável do atributo.
* ##### **<modifiers>**
    * **read**: permite ou não a leitura do atributo, gerando método getter ou não.
    * **write**: permite ou não a escrita do atributo, gerando método setter ou não.
    * **search**: pesquisável usando a FlexibleSearch.
    * **optional**: item não obrigatório.
 * ##### **<persistence>**
    * **type**: *property* ou *dynamic*. *Property* o valor será armazenado no banco e é chamado atributo persistente. *Dynamic* o valor não será armazenado no banco e é chamado de atributo dinâmico. 
    

# Importar projeto Hybris no IntelliJ

### 1. Selecione a opção "Import Project"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_1.png?raw=true)
### 2. Selecione a pasta onde o projeto está armazenado
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_2.png?raw=true)
### 3. Selecione o modelo "hybris"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_3.png?raw=true)
### 4. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_4.png?raw=true)
### 5. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_5.png?raw=true)
### 6. Marque as opções como está mostrado
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_6.png?raw=true)
### 7. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_7.png?raw=true)
### 8. Apenas clique em "Next"
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_8.png?raw=true)
### 9. Caso não haja SDK selecionado, cliquei no + e selecione a pasta onde o JDK está instalado
![Imagem](https://github.com/mathscott/hybris/blob/master/images/intellij_9.png?raw=true)

# Gerar um módulo
Vá até a pasta Hybris/hybris/bin/platform, abra o terminal e rode o seguinte comando:
```sh
ant modulegen
```
Digite develop ou apenas aperte ENTER para selecionar o valor padrão:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/selecione_opcao_ant_modulegen_1.png?raw=true)

Digite accelerator ou apenas aperte ENTER para selecionar o valor padrão:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_2.png?raw=true)

Digite o nome desejado para o seu módulo ou apenas aperte ENTER para selecionar o valor padrão de training:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_3.png?raw=true)

Digite o nome do pacote base para extensões ou apenas aperte ENTER para selecionar o valor padrão (recomendado deixar o valor padrão):
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_4.png?raw=true)

Caso tenha ocorrido tudo certo, a seguinte mensagem deve aparecer:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/ant_modulegen_5.png?raw=true)

# Criar uma extensão
##### 1. Abra o terminal ou cmd e vá até a pasta `CAMINHO_PARA_O_HYBRIS/hybris/bin/platform`.
Digite o comando:
```sh
ant extgen
```
##### 2. Selecione o template ybackoffice e aperte "ENTER":
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_1.png?raw=true)
##### 3. Dê um nome para sua extensão e aperte "ENTER":
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_2.png?raw=true)
##### 4. Escolha o pacote para sua extensão e aperte "ENTER":
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_3.png?raw=true)
##### 5. Não registre como uma extensão SAAS:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_4.png?raw=true)
##### 6. Não crie um widget:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_5.png?raw=true)
##### 7. Não crie uma planilha simples de estilo:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_6.png?raw=true)

##### 8. Adicione a extensão no `localextensions.xml` em `CAMINHO_PARA_O_HYBRIS/hybris/config/localextensions.xml`:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_7.png?raw=true)

##### 9. Adicione a seguinte configuração no arquivo `CAMINHO_PARA_O_HYBRIS/hybris/bin/custom/extensao1/resources/NOME_DA_SUA_EXTENSAO-backoffice-config.xml`, substituindo os valores **"navigation-node id"**, **"type-node id"** e **"type-node code"** com os valores desejados:
```html
<context component="explorer-tree" merge-by="module">
		 <n:explorer-tree xmlns:n="http://www.hybris.com/cockpitng/config/explorertree">
			 <n:navigation-node id="ExtensaoTesteBackOffice">
			 <n:type-node id="Teste" code="Teste" />
			 </n:navigation-node>
		 </n:explorer-tree>
	</context>
	<context type="Teste" component="listview">
		 <list:list-view xmlns:list="http://www.hybris.com/cockpitng/component/listView">
			<list:column qualifier="code"/>
			<list:column qualifier="name"/>
			<list:column qualifier="duration"/>
			<list:column qualifier="amount"/>
		 	</list:list-view>
	</context>
```
Insira dentro da tag **<config>** como mostrado na imagem abaixo:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/extension_8.png?raw=true)

##### 10. Execute os seguintes comandos na pasta `CAMINHO_PARA_O_HYBRIS/hybris/bin/platform`:
```sh
setantenv.bat
```
```sh
ant clean all
```
```sh
hybrisserver.bat
```
##### 11. Abra o https://localhost:9002 -> platform -> update (Selecione somente a extensão "extensao1" na lista) -> update


# Inicializar o Hybris
Após ter criado o pacote para edição, deve-se inicialiar o projeto.
Para inicializá-lo use o seguinte comando:
```sh
$ ant initialize
```

# Compilar o Hybris
Checa a estrutura da pasta e builda caso não exista build.
Vá até a pasta Hybris/hybris/bin/platform, abra o terminal e rode o seguinte comando:
```sh
$ ant clean all
```

Caso dê tudo certo, deve aparecer a seguinte tela:
![Imagem](https://github.com/mathscott/hybris/blob/master/images/finished_clean_all_1.png?raw=true)

# Inicializar o servidor
Após compilar o projeto, inicialize o servidor.
Para Linux, use o seguinte comando na pasta Hybris/hybris/bin/platform:
```sh
$ ./hybrisserver.sh
```
Para Windows, use o seguinte comando na pasta Hybris/hybris/bin/platform:
```sh
$ hybrisserver.bat
```